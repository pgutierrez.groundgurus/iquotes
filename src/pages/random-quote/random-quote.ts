import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuoteService } from '../../service/quote.service';
import { Quote } from '../../model/quote.model';

@IonicPage()
@Component({
  selector: 'page-random-quote',
  templateUrl: 'random-quote.html',
})
export class RandomQuotePage {
  private quotes: Quote[];
  private randomQuote: Quote;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private quoteService: QuoteService) {
  }

  ionViewDidEnter() {
    this.quoteService.getAll()
      .subscribe(res => {
        this.quotes = res;
        this.generateRandomQuote();
      });
  }

  generateRandomQuote() {
    var randomNumber = Math.floor(Math.random() * (this.quotes.length));
    this.randomQuote = this.quotes[randomNumber];
  }
}
