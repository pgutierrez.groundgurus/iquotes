import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Quote } from '../../model/quote.model';

@IonicPage()
@Component({
  selector: 'page-quote-details',
  templateUrl: 'quote-details.html',
})
export class QuoteDetailsPage {
  quote: Quote;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidEnter() {
    var text = this.navParams.get('text');
    var from = this.navParams.get('from');

    this.quote = new Quote(text, from);
  }

}
