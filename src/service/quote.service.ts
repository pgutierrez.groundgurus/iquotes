import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Quote } from '../model/quote.model';

@Injectable()
export class QuoteService {
    constructor(private http: Http) { }

    getAll() : Observable<Quote[]> {
        return this.http.get('assets/data/enterpreneur-quotes.json')
            .map(res => res.json()
                .map(d => {
                    return new Quote(d.text, d.from);
                }));
    }
}